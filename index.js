var log = require('./vendor/mersenne-log')

// Find prime numbers less than or equal to the limit
function sieveOfEratosthenes(limit) {
  var sieve = [];
  var primes = [];
  var k;
  var l;

  sieve[1] = false;

  // Assume all are prime
  for (k = 2; k <= limit; k += 1) {
    sieve[k] = true;
  }

  // Eliminate numbers that are multiples of prime k's
  for (k = 2; k * k <= limit; k += 1) {
    if (sieve[k] !== true) {
      continue;
    }

    log.info("discarding multiples of " + k)

    for (l = k * k; l <= limit; l += k) {
      sieve[l] = false;
    }
  }

  // Summarize primes
  sieve.forEach(function (value, key) {
    if (value) {
      this.push(key);
    }
  }, primes);

  return primes;
};

module.exports = {
  sieveOfEratosthenes
};
